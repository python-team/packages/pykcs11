pykcs11 (1.5.17-1) unstable; urgency=medium

  * New upstream version
  * d/control: Standards-Version: 4.6.2 -> 4.7.0. No change needed.

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 16 Oct 2024 13:50:45 +0200

pykcs11 (1.5.16-1) unstable; urgency=medium

  * New upstream version

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 24 May 2024 17:58:22 +0200

pykcs11 (1.5.15-1) unstable; urgency=medium

  * New upstream version

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 17 Apr 2024 15:59:04 +0200

pykcs11 (1.5.14-1) unstable; urgency=medium

  * New upstream version
  * Remove d/p/get_PYKCS11LIB included upstream

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 03 Feb 2024 18:58:02 +0100

pykcs11 (1.5.13-1) unstable; urgency=medium

  * New upstream version
  * Add d/p/get_PYKCS11LIB: add missing get_PYKCS11LIB.py
  * Do not run tests at build time
  * reformat Build-Depends:
  * Add pybuild-plugin-pyproject to Build-Depends:

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 07 Jan 2024 15:12:07 +0100

pykcs11 (1.5.12-2) unstable; urgency=medium

  * d/rules: extend clean target to remove _LowLevel.*.so file
    (Closes: #1047917)
  * d/source/options: ignore changes on generated egg files

 -- Ludovic Rousseau <rousseau@debian.org>  Mon, 14 Aug 2023 15:18:49 +0200

pykcs11 (1.5.12-1) unstable; urgency=medium

  * New upstream version
  * d/control: Standards-Version: 4.6.1 -> 4.6.2. No change needed.
  * add autopkgtests

 -- Ludovic Rousseau <rousseau@debian.org>  Mon, 10 Apr 2023 12:35:03 +0200

pykcs11 (1.5.11-1) unstable; urgency=medium

  * New upstream version
  * d/control: Standards-Version: 4.5.1 -> 4.6.1. No change needed.

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 04 Sep 2022 17:05:26 +0200

pykcs11 (1.5.10-2) unstable; urgency=medium

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Debian Janitor ]
  * debian/copyright: Replace commas with whitespace to separate items in Files
    paragraph.
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit, Name,
    Repository, Repository-Browse.

 -- Sandro Tosi <morph@debian.org>  Thu, 02 Jun 2022 21:23:17 -0400

pykcs11 (1.5.10-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Ludovic Rousseau ]
  * New upstream version
  * d/watch: upgrade version from 3 to 4
  * d/control: Standards-Version: 4.5.0 -> 4.5.1. No change needed.

 -- Ludovic Rousseau <rousseau@debian.org>  Thu, 31 Dec 2020 15:52:46 +0100

pykcs11 (1.5.9-1) unstable; urgency=medium

  * New upstream version

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 31 Jul 2020 15:51:25 +0200

pykcs11 (1.5.8-1) unstable; urgency=medium

  * New upstream version
  * d/control: Standards-Version: 4.4.1 -> 4.5.0. No change needed.
  * d/control: Move from debhelper-compat 12 to 13

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 15 May 2020 16:31:45 +0200

pykcs11 (1.5.7-1) unstable; urgency=medium

  * New upstream version
  * d/control: upgrade debhelper from 11 to 12. Fix lintian warning:
    P: pykcs11 source: package-uses-old-debhelper-compat-version 11
  * d/control: use "Rules-Requires-Root: no". Fix lintian warning:
    P: pykcs11 source: rules-requires-root-missing

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 18 Dec 2019 18:32:09 +0100

pykcs11 (1.5.6-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Ludovic Rousseau ]
  * New upstream version

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 18 Dec 2019 17:52:31 +0100

pykcs11 (1.5.5-3) unstable; urgency=medium

  * new upload, no change

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 04 Aug 2019 18:09:45 +0200

pykcs11 (1.5.5-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Ludovic Rousseau ]
  * rebuild for source only upload

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 04 Aug 2019 17:46:26 +0200

pykcs11 (1.5.5-1) unstable; urgency=medium

  * New upstream release
  * d/control: Build-Depends: python3-setuptools
  * d/upstream/signing-key.asc: remove extra signatures
  * d/control: Standards-Version: 4.2.1 -> 4.3.0. No change needed.

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 17 Apr 2019 18:03:07 +0200

pykcs11 (1.5.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout
  * d/watch: Use https protocol

  [ Ludovic Rousseau ]
  * new upstream release
  * d/control: Standards-Version: 3.9.8 -> 4.2.1. No change needed.
  * d/compat: move from 10 to 11

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 12 Oct 2018 16:07:53 +0200

pykcs11 (1.5.2-1) unstable; urgency=medium

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Thu, 12 Apr 2018 16:09:48 +0200

pykcs11 (1.5.1-2) unstable; urgency=medium

  [ Piotr Ożarowski ]
  * Add dh-python to Build-Depends

 -- Ludovic Rousseau <rousseau@debian.org>  Thu, 12 Apr 2018 15:40:01 +0200

pykcs11 (1.5.1-1) unstable; urgency=medium

  * New upstream release
  * d/rules: only call setup.py to simplify the build

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 30 Mar 2018 11:16:22 +0200

pykcs11 (1.5.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Ludovic Rousseau ]
  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Mon, 26 Mar 2018 21:52:47 +0200

pykcs11 (1.4.4-2) unstable; urgency=medium

  * Upgrade to debhelper compat 10 (from 9)
  * Use hardening=+all
  * debian/watch: check PGP signature

 -- Ludovic Rousseau <rousseau@debian.org>  Thu, 04 Jan 2018 21:47:11 +0100

pykcs11 (1.4.4-1) unstable; urgency=medium

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 11 Oct 2017 14:43:38 +0200

pykcs11 (1.4.3-1) unstable; urgency=medium

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 24 Jun 2017 16:30:47 +0200

pykcs11 (1.4.2-1) unstable; urgency=medium

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 21 May 2017 15:24:43 +0200

pykcs11 (1.4.1-1) unstable; urgency=medium

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 11 Feb 2017 14:32:14 +0100

pykcs11 (1.4.0-1) unstable; urgency=medium

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 04 Feb 2017 18:33:05 +0100

pykcs11 (1.3.3-1) unstable; urgency=medium

  * New upstream release
  * debian/control: Standards-Version: 3.9.6 -> 3.9.8. No change needed.

 -- Ludovic Rousseau <rousseau@debian.org>  Tue, 01 Nov 2016 17:02:19 +0100

pykcs11 (1.3.2-1) unstable; urgency=medium

  * new upstream version

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 27 Jan 2016 20:28:17 +0100

pykcs11 (1.3.1-2) unstable; urgency=medium

  [ Scott Kitterman ]
  * Team upload.
  * Build for all supported python3 versions (this is better than the fix
    proposed in #793553)
    - Change python3-dev build-dep back to python3-all-dev
    - Update debian/rules to loop over the list of supported python3 versions
      during build/install (debian/rules is now doing explicitly for each
      python3 what it was doing implicitly via the makefile before)

 -- Ludovic Rousseau <rousseau@debian.org>  Thu, 31 Dec 2015 18:44:20 +0100

pykcs11 (1.3.1-1) unstable; urgency=medium

  * new upstream version

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 23 Oct 2015 11:57:38 +0200

pykcs11 (1.3.0-2) unstable; urgency=medium

  * Fix "Please replace python3-all-dev build-dependency with python3-
    dev" proposed patch applied (Closes: #793553)
  * debian/copyright: do not duplicate the GNU GPL v2 license
  * debian/control: Standards-Version: 3.9.5 -> 3.9.6. No change needed.
  * debian/control: remove David Smith from Uploaders: I do not remember him
    doing an upload.

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 09 Aug 2015 15:55:50 +0200

pykcs11 (1.3.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ludovic Rousseau ]
  * update Homepage: field
  * New upstream release
  * use dh compatibility level 9 to get hardening
  * move to Python 3.x
  * debian/control: Standards-Version: 3.9.3 -> 3.9.5. No change needed.

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 26 Jul 2014 13:47:26 +0200

pykcs11 (1.2.4-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version: 3.9.2 -> 3.9.3. No change needed.

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 21 Apr 2012 10:02:35 +0200

pykcs11 (1.2.3-6) unstable; urgency=low

  * Fix "Can't be found with "apt-cache search pkcs11 python""
    Add a Keywords: line in the package long description (Closes: #636163)

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 05 Aug 2011 17:02:58 +0200

pykcs11 (1.2.3-5) unstable; urgency=low

  * rebuild to add support of python 2.7 and remove support of python 2.5
  * debian/control: Standards-Version: 3.9.1 -> 3.9.2, no change needed.

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 23 Apr 2011 16:26:53 +0200

pykcs11 (1.2.3-4) unstable; urgency=low

  * debian/rules: use a minimal version

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 23 Apr 2011 16:22:05 +0200

pykcs11 (1.2.3-3) unstable; urgency=low

  * Fix "deprecation of dh_pycentral, please use dh_python2" by using
    dh_python2 (Closes: #616969)
  * debian/rules: add --install-layout=deb

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 09 Mar 2011 18:54:41 +0100

pykcs11 (1.2.3-2) unstable; urgency=low

  * upload to unstable

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 09 Feb 2011 23:20:50 +0100

pykcs11 (1.2.3-1) experimental; urgency=low

  * New upstream release
  * debian/control: Standards-Version: 3.8.4 -> 3.9.1, no change needed

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 03 Dec 2010 14:36:09 +0100

pykcs11 (1.2.2-1) unstable; urgency=low

  * New upstream release
  * debian/control: Fix lintian I: pykcs11 source:
    binary-control-field-duplicates-source field "section" in package
    python-pykcs11
  * debian/control: Standards-Version: 3.8.3 -> 3.8.4, no change needed
  * debian/source/format: change from "1.0" to "3.0 (quilt)" to easily patch
    the package

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 16 Jun 2010 09:29:15 +0200

pykcs11 (1.2.1-3) unstable; urgency=low

  * debian/rules: allow to build without python2.4 (and with python2.6)
  * debian/control: Standards-Version: 3.8.1 -> 3.8.3. no change needed.

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 14 Nov 2009 18:35:51 +0100

pykcs11 (1.2.1-2) unstable; urgency=low

  * Fix "Missing watch file" add debian/watch (Closes: #520200)
  * use debhelper V7
  * update Standards-Version: from 3.7.2 to 3.8.1
    add support of parallel and nostrip in DEB_BUILD_OPTIONS

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 18 Mar 2009 14:49:15 +0000

pykcs11 (1.2.1-1) unstable; urgency=low

  * New upstream release
   - use pkcs11.h from OpenSC instead of (non-free) rsaref
  * debian/copyright: add licence for src/opensc/* and remove licence for
    src/rsaref/*

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 14 Nov 2008 19:18:39 +0000

pykcs11 (1.2.0-1) unstable; urgency=low

  * New upstream release

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 30 Aug 2008 13:02:36 +0000

pykcs11 (1.1.1-1) unstable; urgency=low

  * Initial release (Closes: #489369).  ITP: python-pykcs11 -- a complete RSA
    PKCS#11 wrapper for Python

 -- David Smith <davidsmith@acm.org>  Sat, 05 Jul 2008 20:28:46 +0900
